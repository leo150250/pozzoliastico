# Pozzoliástico

Um pequeno gerador aleatório de partituras, apenas para exercícios de divisão simples. Serve muito bem para gincanas musicais!

Versão pública estável disponível em: [leandrogabriel.net/sistemas/pozzoliastico/](url) (Versão 0.2)