var compassoTempos=0;
var compassoSemibreve=0;
var temposPreenchidos=0;
var compassosPreenchidos=0;
var compassosTotal=2;
var div_pauta=document.getElementById("pauta");
var select_andamento=document.getElementById("andamento");
var stringPauta="";
function novoAndamento() {
	if (select_andamento.value==0) {
		var chance=Math.round(Math.random()*10);
		console.log({chance});
		switch (chance) {
			case 0: compassoTempos=2; compassoSemibreve=4; break;
			case 1: compassoTempos=3; compassoSemibreve=4; break;
			case 2: compassoTempos=4; compassoSemibreve=4; break;
			case 3: compassoTempos=5; compassoSemibreve=4; break;
			case 4: compassoTempos=6; compassoSemibreve=4; break;
			case 5: compassoTempos=2; compassoSemibreve=2; break;
			case 6: compassoTempos=3; compassoSemibreve=2; break;
			case 7: compassoTempos=3; compassoSemibreve=8; break;
			case 8: compassoTempos=6; compassoSemibreve=8; break;
			case 9: compassoTempos=9; compassoSemibreve=8; break;
			case 10: compassoTempos=12; compassoSemibreve=8; break;
		}
	} else if (select_andamento.value==1) {
		compassoTempos=4;
		compassoSemibreve=4;
	} else if (select_andamento.value==2) {
		compassoTempos=3;
		compassoSemibreve=4;
	} else if (select_andamento.value==3) {
		compassoTempos=6;
		compassoSemibreve=8;
	}
	stringPauta+="<div class=\"andamento\"><img src=\"img/partitura_formula"+compassoTempos+".png\"><img src=\"img/partitura_formula"+compassoSemibreve+".png\"></div>";
}
function novaFigura() {
	var figura="";
	var chance=Math.round(Math.random()*10);
	switch (chance) {
		case 0: figura="Sb"; break;
		case 1: figura="M"; break;
		case 2: figura="M"; break;
		case 3: figura="Sm"; break;
		case 4: figura="Sm"; break;
		case 5: figura="Sm"; break;
		case 6: figura="Sm"; break;
		case 7: figura="C"; break;
		case 8: figura="C"; break;
		case 9: figura="C"; break;
		case 10: figura="C"; break;
	}
	var figuraOk=false;
	while (!figuraOk) {
		var valorFigura=0;
		switch (figura) {
			case "Sb": valorFigura=compassoSemibreve; break;
			case "M": valorFigura=compassoSemibreve/2; break;
			case "Sm": valorFigura=compassoSemibreve/4; break;
			case "C": valorFigura=compassoSemibreve/8; break;
			case "Sc": valorFigura=compassoSemibreve/16; break;
		}
		if (temposPreenchidos+valorFigura<=compassoTempos) {
			temposPreenchidos+=valorFigura;
			console.log("Inserido!");
			console.log({temposPreenchidos});
			figuraOk=true;
		} else {
			console.log("Extrapolou!");
			switch (figura) {
				case "Sb": figura="M"; break;
				case "M": figura="Sm"; break;
				case "Sm": figura="C"; break;
				case "C": figura="Sc"; break;
			}
		}
	}
	var chance=Math.round(Math.random()*10);
	if (chance<=2) {
		figura+="Pausa";
	}
	stringPauta+="<img class=\"figura\" src=\"img/partitura_fig"+figura+".png\">";
	if (temposPreenchidos>=compassoTempos) {
		console.log({compassoTempos});
		stringPauta+="<img class=\"barra\" src=\"img/partitura_pautaBarra.png\">";
		temposPreenchidos=0;
		compassosPreenchidos++;
	}
}
function gerarNovaPauta() {
	temposPreenchidos=0;
	compassosPreenchidos=0;
	stringPauta="";
	stringPauta+="<img class=\"clave\" src=\"img/partitura_claveG.png\">";
	novoAndamento();
	while (compassosPreenchidos<compassosTotal) {
		novaFigura();
	}
	stringPauta+="<img class=\"barra\" src=\"img/partitura_pautaBarraFinal.png\">";
	div_pauta.innerHTML=stringPauta;
}

//Do metrônomo:
var metronomoRodando=false;
var brilhoClick=0;
var input_bpm=document.getElementById("bpm");
var audio_bpmClick=document.getElementById("bpmClick");
var canvas_bpmVisor=document.getElementById("bpmVisor");
var ctx_bpmVisor=canvas_bpmVisor.getContext("2d");
ctx_bpmVisor.fillStyle="#00FF00"
//var canvas_bpmProgresso=document.getElementById("bpmProgresso");
var metronomo="";
var bpmInicio=null;
var bpmPrepara=0;
function iniciaMetronomo() {
	if (!metronomoRodando) {
		metronomoRodando=true;
		bpmInicio=Date.now();
		//console.log(bpmInicio);
		metronomo=setInterval(bipMetronomo,1);
	}
}
function paraMetronomo() {
	if (metronomoRodando) {
		metronomoRodando=false;
		clearInterval(metronomo);
		//canvas_bpmProgresso.style.width="0px";
		ctx_bpmVisor.clearRect(0,0,100,1);
		metronomo="";
		bpmInicio=null;
		bpmPrepara=0;
		brilhoClick=0;
		document.body.style.backgroundColor="#DDD";
		document.body.style.transition=((60/input_bpm.value)*500)+"ms linear";
	}
}
function bipMetronomo() {
	//console.log((60/input_bpm.value)*1000);
	var passoBPM=Date.now() % ((60/input_bpm.value)*1000);
	ctx_bpmVisor.fillRect(0,0,((passoBPM/((60/input_bpm.value)*1000))*100),1);
	//canvas_bpmProgresso.style.width=((passoBPM/((60/input_bpm.value)*1000))*16)+"px";
	console.log(passoBPM);
	if (passoBPM>bpmPrepara) {
		bpmPrepara=passoBPM;
	} else {
		audio_bpmClick.currentTime=0;
		audio_bpmClick.play();
		bpmPrepara=0;
		ctx_bpmVisor.clearRect(0,0,100,1);
		brilhoClick=5;
		console.log(Date.now());
	}
	if (brilhoClick>0) {
		document.body.style.transition="0ms linear";
		document.body.style.backgroundColor="#FFF";
		brilhoClick--;
	} else {
		document.body.style.backgroundColor="#DDD";
		document.body.style.transition=((60/input_bpm.value)*500)+"ms linear";
	}
}

document.body.style.backgroundColor="#DDD";
//gerarNovaPauta();